extends Node


@export
var stage_scene: PackedScene

@export
var title: TitleScreen

@export
var credits: CreditsScreen

@export
var game_over: GameOverScreen

@export
var stage_ui: StageUI

@export
var flash_message: FlashMessage

@export
var volume_control: VolumeControl

@export
var pause_screen: PauseScreen

var stage_current: Stage

var _pausable := false


func _ready():
	self.clear_screen()
	self.go_to_title()


func _process(_delta):
	if Input.is_action_just_pressed("pause"):
		self.set_paused(not self.get_tree().paused)
	
	if Input.is_action_just_pressed("volume_up"):
		self.increase_volume(1)
	if Input.is_action_just_pressed("volume_down"):
		self.increase_volume(-1)
	if Input.is_action_just_pressed("volume_mute"):
		self.increase_volume(-10)


func go_to_stage():
	self.clear_screen()
	
	if stage_scene and stage_scene.can_instantiate():
		var stage = self.stage_scene.instantiate() as Stage
		stage.game_over.connect(self.show_game_over)
		stage.points_changed.connect(self.stage_ui.set_points)
		stage.speed_increased.connect(func(_message):
			self.flash_message.flash("Getting faster!"))
		self.stage_current = stage
		self.add_child(stage)
		
		self.stage_ui.show()
		
		self._pausable = true
	else:
		push_warning("Unable to instantiate `stage_scene`")


func go_to_title():
	self.clear_screen()
	
	if is_instance_valid(title):
		self.title.show()
		self.title.focus()
		self._pausable = false
	else:
		push_warning("Unable to instantiate `title`")


func go_to_credits():
	self.clear_screen()
	
	if is_instance_valid(credits):
		self.credits.show()
		self.credits.focus()
		self._pausable = false
	else:
		push_warning("Unable to instantiate `credits`")


func show_game_over(points: int):
	self.stage_current.points_changed.disconnect(self.stage_ui.set_points)
	if is_instance_valid(game_over):
		self.game_over.points = points
		self.game_over.show()
		self.game_over.focus()
		self._pausable = false
	else:
		push_warning("Unable to instantiate `game_over`")


func hide_game_over():
	if is_instance_valid(game_over):
		self.game_over.hide()


func set_paused(paused: bool):
	if (self._pausable and paused) or not paused:
		self.get_tree().paused = paused
		self.pause_screen.visible = paused
		if paused:
			self.pause_screen.focus()


func increase_volume(amount: int):
	var bus_idx := 0
	
	var volume_increment := 0.1
	
	var volume_current := db_to_linear(AudioServer.get_bus_volume_db(bus_idx))
	var volume_new := clampf(volume_current + (amount * volume_increment), 0, 1)
	
	AudioServer.set_bus_volume_db(bus_idx, linear_to_db(volume_new))
	
	self.volume_control.volume = volume_new


func clear_screen():
	self.hide_game_over()
	self.set_paused(false)
	
	if is_instance_valid(title):
		self.title.hide()
	
	if is_instance_valid(credits):
		self.credits.hide()
	
	if is_instance_valid(stage_ui):
		self.stage_ui.hide()
	
	if self.stage_current and is_instance_valid(self.stage_current):
		self.stage_current.queue_free()
