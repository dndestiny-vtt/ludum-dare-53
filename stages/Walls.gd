@tool
extends Node2D


const LINE_WIDTH := 1.0
const DISTANCE_UNIT := 64.0

@export
var size: Vector2

## The length of the side wall lines before the back wall is hit
@export
var length: int

@export
var color: Color

@export
var color_line: Color

## The distance travelled
@export
var distance := 0

## Number of side wall lines to draw
@export
var side_line_count := 5


func _draw():
	var top_left := Vector2.ZERO
	var top_right := Vector2(self.size.x, 0)
	var bottom_left := Vector2(0, self.size.y)
	var bottom_right := self.size
	
	var back_top_left := top_left + top_left.direction_to(bottom_right) * self.length
	var back_top_right := top_right + top_right.direction_to(bottom_left) * self.length
	var back_bottom_left := bottom_left + bottom_left.direction_to(top_right) * self.length
	var back_bottom_right := bottom_right + bottom_right.direction_to(top_left) * self.length
	
	# Side walls
	self.draw_line(top_left, back_top_left, self.color, self.LINE_WIDTH)
	self.draw_line(top_right, back_top_right, self.color, self.LINE_WIDTH)
	self.draw_line(bottom_left, back_bottom_left, self.color, self.LINE_WIDTH)
	self.draw_line(bottom_right, back_bottom_right, self.color, self.LINE_WIDTH)
	
	# Back wall
	self.draw_line(back_top_left, back_top_right, self.color, self.LINE_WIDTH)
	self.draw_line(back_top_right, back_bottom_right, self.color, self.LINE_WIDTH)
	self.draw_line(back_bottom_right, back_bottom_left, self.color, self.LINE_WIDTH)
	self.draw_line(back_bottom_left, back_top_left, self.color, self.LINE_WIDTH)
	
	# Road lines
	var left_line_back := \
			back_bottom_left + back_bottom_left.direction_to(back_bottom_right) \
			* floori(back_bottom_left.distance_to(back_bottom_right) / 3)
	var right_line_back := \
			back_bottom_right + back_bottom_right.direction_to(back_bottom_left) \
			* floori(back_bottom_right.distance_to(back_bottom_left) / 3)
	var left_line_front := \
			bottom_left + bottom_left.direction_to(bottom_right) \
			* floori(bottom_left.distance_to(bottom_right) / 3)
	var right_line_front := \
			bottom_right + bottom_right.direction_to(bottom_left) \
			* floori(bottom_right.distance_to(bottom_left) / 3)
	
	self.draw_line(
		left_line_back,
		left_line_front.move_toward(left_line_back, -200),
		self.color_line)
	self.draw_line(
		right_line_back,
		right_line_front.move_toward(right_line_back, -200),
		self.color_line)
	
	# Side wall lines
	var centre := (self.size / 2).floor()
	
	for i in range(self.side_line_count):
		var side_line_distance := (i * self.DISTANCE_UNIT) - (self.distance % floori(self.DISTANCE_UNIT))
		var side_line_distance_ratio := 1.0 / ((side_line_distance / self.DISTANCE_UNIT) + 1)
		
		var side_line_top := \
			top_left + (centre - top_left) * (1 - side_line_distance_ratio)
		var side_line_bottom := \
			bottom_left + (centre - bottom_left) * (1 - side_line_distance_ratio)
		
		self.draw_line(side_line_top, side_line_bottom, self.color_line)
	
	for i in range(self.side_line_count):
		var side_line_distance := (i * self.DISTANCE_UNIT) - (self.distance % floori(self.DISTANCE_UNIT))
		var side_line_distance_ratio := 1.0 / ((side_line_distance / self.DISTANCE_UNIT) + 1)
		
		var side_line_top := \
			top_right + (centre - top_right) * (1 - side_line_distance_ratio)
		var side_line_bottom := \
			bottom_right + (centre - bottom_right) * (1 - side_line_distance_ratio)
		
		self.draw_line(side_line_top, side_line_bottom, self.color_line)


func _process(_delta):
	self.queue_redraw()
