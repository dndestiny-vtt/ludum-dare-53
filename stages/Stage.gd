extends Node2D
class_name Stage


const LORRY_LANES_X := [80, 270, 460]

## The minimum distance the lorry has to be from the target position before it
## stops.
const LORRY_DISTANCE_MIN := 1.0

signal game_over(points)
signal points_changed(points)
signal speed_increased(speed)

@onready
var Lorry := $Lorry as Node2D

@onready
var Walls := $Walls as Node2D

@onready
var Music := $Music as AudioStreamPlayer

@onready
var DeliveredSound := $DeliveredSound as AudioStreamPlayer

@onready
var DeathSound := $DeathSound as AudioStreamPlayer

@export
var Barrier: Script

@export
var Postbox: Script

@export
var barrier_interval := 256

@export
var barrier_spawn_distance := 512

@export
var postbox_interval := 8 * 64

@export
var postbox_spawn_offset := 2 * 64

@export
var speed := 5

@export
var speed_max := 16

@export
var accelerate_interval := 1

@export
var accelerate_distance := 4096

@export
var shooting_range := Vector2(-32, 64)

var lane_current := 1:
	set(value):
		lane_current = clamp(value, 0, self.LORRY_LANES_X.size() - 1)

## Dictionary{Barrier, lane}
var barriers := {} as Dictionary

## Dictionary{Postbox, wall}
var postboxes := {} as Dictionary

var finished := false

var distance := 0

var packets_delivered := 0

var _last_barrier_distance := 0
var _last_accelerate_distance := 0
var _last_postbox_distance := 0

var _spawned_first_postbox := false

var _target_lorry_position_x: float


func _ready():
	self.Lorry.position.x = self.LORRY_LANES_X[self.lane_current]
	_target_lorry_position_x = Lorry.position.x


func _process(_delta):
	self.distance += speed
	self.Walls.distance = self.distance
	
	for barrier in self.barriers.keys():
		if barrier.distance <= -64:
			self.barriers.erase(barrier)
			barrier.queue_free()
		
		barrier.distance += -speed
	
	for postbox in self.postboxes.keys():
		if postbox.distance <= -32:
			self.postboxes.erase(postbox)
			postbox.queue_free()
#
		postbox.distance += -speed
	
	if not self.finished:
		if Input.is_action_just_pressed("move_left"):
			self.lane_current += -1
			_target_lorry_position_x = self.LORRY_LANES_X[self.lane_current]
		if Input.is_action_just_pressed("move_right"):
			self.lane_current += 1
			_target_lorry_position_x = self.LORRY_LANES_X[self.lane_current]
		
		if Input.is_action_just_pressed("shoot_red"):
			self.shoot()
		
		if abs(Lorry.position.x - _target_lorry_position_x) > LORRY_DISTANCE_MIN:
			Lorry.position.x = lerp(Lorry.position.x, _target_lorry_position_x, 0.5)
		
		if self.distance - self._last_barrier_distance >= self.barrier_interval:
			self.spawn_barrier(
				randi_range(0, self.LORRY_LANES_X.size() - 1),
				snappedi(self.barrier_spawn_distance, self.barrier_interval))
			self._last_barrier_distance = snappedi(self.distance, self.barrier_interval)
		
		for barrier in self.barriers.keys():
			if barrier.distance < 0 and barrier.distance >= 0 - self.speed and self.barriers[barrier] == self.lane_current:
				self.finished = true
				self.Lorry.visible = false
				self.Music.stop()
				self.DeathSound.play()
				self.game_over.emit(self.points())
		
		if not self._spawned_first_postbox:
			if self.distance > self.postbox_spawn_offset:
				self.spawn_postbox(randi_range(0, 1))
				self._last_postbox_distance = snappedi(self.distance, self.postbox_interval) + self.postbox_spawn_offset
				self._spawned_first_postbox = true
		elif self.distance - self._last_postbox_distance >= self.postbox_interval:
			self.spawn_postbox(randi_range(0, 1))
			self._last_postbox_distance = snappedi(self.distance, self.postbox_interval) + self.postbox_spawn_offset
		
		if self.speed < self.speed_max \
				and self.distance - self._last_accelerate_distance >= self.accelerate_distance:
			self.accelerate(self.accelerate_interval)
	
	self.points_changed.emit(self.points())


func shoot():
	var wall := -1
	if self.lane_current == 0:
		wall = 0
	elif self.lane_current == 2:
		wall = 1
	
	var aiming := wall >= 0
	if aiming:
		self.postboxes.keys() \
			.filter(func(postbox):
				return postboxes[postbox] == wall) \
			.filter(func(postbox):
				return postbox.distance >= self.shooting_range.x \
				and postbox.distance < self.shooting_range.y) \
			.map(func(postbox):
				self.packets_delivered += 1
				self.postboxes.erase(postbox)
				postbox.queue_free()
				self.DeliveredSound.play())


func accelerate(amount: int):
	self.speed = min(self.speed + amount, self.speed_max)
	self._last_accelerate_distance = \
		snappedi(self.distance, self.accelerate_distance)
	self.speed_increased.emit(self.speed)


func spawn_barrier(lane: int, distance: int):
	var barrier := Node2D.new()
	barrier.set_script(self.Barrier)
	barrier = barrier as Barrier
	
	barrier.size = Vector2(213, 100)
	barrier.position = Vector2(
		self.Walls.position.x + (float(lane) / self.LORRY_LANES_X.size()) * self.Walls.size.x,
		self.Walls.position.y + self.Walls.size.y - barrier.size.y)
	barrier.distance = 512
	barrier.color = Color.YELLOW
	barrier.vanishing_point = Vector2(320, 180)
	barrier.distance_ratio_devisor = 64
	barrier.fill_line_interval = 32
	
	self.barriers[barrier] = lane
	self.add_child(barrier)


func spawn_postbox(wall: int):
	var postbox := Node2D.new()
	postbox.set_script(self.Postbox)
	postbox = postbox as Postbox
	
	postbox.position = Vector2(wall * 640, 100)
	postbox.distance = 464 # 512+64+16 to centre it
	
	self.postboxes[postbox] = wall
	self.add_child(postbox)


func points() -> int:
	return \
		(self.packets_delivered * 10) \
		+ floori(self.distance / 64)
