@tool
extends Node2D
class_name Postbox


@export
var size := Vector3(0, 160, 32)

@export
var distance := 0

@export
var color := Color.RED

@export
var vanishing_point := Vector2(320, 180)

@export
var distance_ratio_devisor := 64.0


func _draw():
	var vanishing_point_global = self.vanishing_point - self.global_position
	
	var back_top := self.projected(Vector3(0, 0, self.size.z + self.distance), vanishing_point_global)
	var back_bottom := self.projected(Vector3(0, self.size.y, self.size.z + self.distance), vanishing_point_global)
	var front_top := self.projected(Vector3(0, 0, 0 + self.distance), vanishing_point_global)
	var front_bottom := self.projected(Vector3(0, self.size.y, 0 + self.distance), vanishing_point_global)
	
	self.draw_line(front_top, back_top, self.color)
	self.draw_line(back_top, back_bottom, self.color)
	self.draw_line(back_bottom, front_bottom, self.color)
	self.draw_line(front_bottom, front_top, self.color)


func _process(_delta):
	self.queue_redraw()


func projected(point: Vector3, vanishing_point: Vector2) -> Vector2:
	var xy := Vector2(point.x, point.y)
	var dr := distance_ratio(point.z)
	return xy + (vanishing_point - xy) * dr


func distance_ratio(distance: float) -> float:
	var distance_ratio_pre: float
	if distance / distance_ratio_devisor == 0:
		distance_ratio_pre = 0.0001
	else:
		distance_ratio_pre = distance / distance_ratio_devisor
	
	return 1.0 - (1.0 / (distance_ratio_pre + 1))
