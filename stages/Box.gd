@tool
extends Node2D
class_name Box


const LINE_WIDTH := -1.0

@export
var vanishing_point_global := Vector2(-100, -50)

@export
var size := Vector3(0, 0, 0)

@export
var position_z := 0.0

@export
var color := Color.RED


func _process(_delta):
	queue_redraw()


func _draw():
	var vanishing_point = vanishing_point_global - global_position
	
	var offset := Vector3(0, 0, position_z)
	
	# n = near ; f = far
	# t = top  ; b = bottom
	# l = left ; r = right
	#
	# Example: `vertex_nbr` would be the closest point to the camera on the
	# bottom right. i.e. coordinates (1, 1, 0)
	var vertex_ntl := project(Vector3(     0,      0,      0) + offset, vanishing_point)
	var vertex_ntr := project(Vector3(size.x,      0,      0) + offset, vanishing_point)
	var vertex_nbl := project(Vector3(     0, size.y,      0) + offset, vanishing_point)
	var vertex_nbr := project(Vector3(size.x, size.y,      0) + offset, vanishing_point)
	var vertex_ftl := project(Vector3(     0,      0, size.z) + offset, vanishing_point)
	var vertex_ftr := project(Vector3(size.x,      0, size.z) + offset, vanishing_point)
	var vertex_fbl := project(Vector3(     0, size.y, size.z) + offset, vanishing_point)
	var vertex_fbr := project(Vector3(size.x, size.y, size.z) + offset, vanishing_point)
	
	# Near square
	self.draw_line(vertex_ntl, vertex_ntr, self.color, LINE_WIDTH)
	self.draw_line(vertex_ntr, vertex_nbr, self.color, LINE_WIDTH)
	self.draw_line(vertex_nbr, vertex_nbl, self.color, LINE_WIDTH)
	self.draw_line(vertex_nbl, vertex_ntl, self.color, LINE_WIDTH)
	
	# Far square
	self.draw_line(vertex_ftl, vertex_ftr, self.color, LINE_WIDTH)
	self.draw_line(vertex_ftr, vertex_fbr, self.color, LINE_WIDTH)
	self.draw_line(vertex_fbr, vertex_fbl, self.color, LINE_WIDTH)
	self.draw_line(vertex_fbl, vertex_ftl, self.color, LINE_WIDTH)
	
	# Connecting lines
	self.draw_line(vertex_ntl, vertex_ftl, self.color, LINE_WIDTH)
	self.draw_line(vertex_ntr, vertex_ftr, self.color, LINE_WIDTH)
	self.draw_line(vertex_nbr, vertex_fbr, self.color, LINE_WIDTH)
	self.draw_line(vertex_nbl, vertex_fbl, self.color, LINE_WIDTH)


func project(point: Vector3, vanishing_point: Vector2) -> Vector2:
	var xy := Vector2(point.x, point.y)
	var distance_ratio := 1.0 - (1.0 / (point.z + 1))
	
	var projected_point := xy + (vanishing_point - xy) * distance_ratio
	return projected_point
