@tool
extends Node2D
class_name Barrier


@export
var size: Vector2

@export
var distance: int

@export
var color: Color

@export
var vanishing_point: Vector2

@export
var distance_ratio_devisor: float

@export
var fill_line_interval: int


func _draw():
	var vanishing_point_global = self.vanishing_point - self.global_position
	
	var top_left_origin := Vector2.ZERO
	var top_right_origin := Vector2(self.size.x, 0)
	var bottom_right_origin := self.size
	var bottom_left_origin := Vector2(0, self.size.y)
	
	var distance_ratio := 1.0 / ((distance / distance_ratio_devisor) + 1)
	
	var top_left := \
			top_left_origin \
			+ (vanishing_point_global - top_left_origin) \
			* (1.0 - distance_ratio) as Vector2
	var top_right := \
			top_right_origin \
			+ (vanishing_point_global - top_right_origin) \
			* (1.0 - distance_ratio) as Vector2
	var bottom_right := \
			bottom_right_origin \
			+ (vanishing_point_global - bottom_right_origin) \
			* (1.0 - distance_ratio) as Vector2
	var bottom_left := \
			bottom_left_origin \
			+ (vanishing_point_global - bottom_left_origin) \
			* (1.0 - distance_ratio) as Vector2
	
	# Box
	self.draw_line(top_left, top_right, self.color)
	self.draw_line(top_right, bottom_right, self.color)
	self.draw_line(bottom_right, bottom_left, self.color)
	self.draw_line(bottom_left, top_left, self.color)
	
	# Fill
	for i in range(fill_line_interval, self.size.x + self.size.y, fill_line_interval):
		var fill_line_top_origin := Vector2(
			min(i, self.size.x),
			max(0, i - self.size.x))
		var fill_line_bottom_origin := Vector2(
			max(0, i - self.size.y),
			min(i, self.size.y))
	
		var fill_line_top := \
				fill_line_top_origin \
				+ (vanishing_point_global - fill_line_top_origin) \
				* (1.0 - distance_ratio) as Vector2
		var fill_line_bottom := \
				fill_line_bottom_origin \
				+ (vanishing_point_global - fill_line_bottom_origin) \
				* (1.0 - distance_ratio) as Vector2
		
		self.draw_line(fill_line_top, fill_line_bottom, self.color)


func _process(_delta):
	self.queue_redraw()
