extends Control
class_name StageUI


const POINTS_COUNTER_TEXT := "Points - %d"

@export
var point_counter_label: Label

var points := 0: set = set_points


func set_points(value):
	points = value
	self.point_counter_label.text = POINTS_COUNTER_TEXT % points
