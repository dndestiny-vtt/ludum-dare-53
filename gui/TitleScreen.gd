extends Control
class_name TitleScreen


signal start_pressed
signal credits_pressed

@export
var FocusButton: Button


func focus():
	FocusButton.grab_focus()
