extends MarginContainer
class_name VolumeControl


@export
var volume_slider: Slider

@export
var animation: AnimationPlayer

var volume := 1.0:
	set(value):
		volume = clamp(value, 0, 1)
		volume_slider.value = value
		
		if animation.is_playing():
			animation.stop()
		animation.play("flash")
