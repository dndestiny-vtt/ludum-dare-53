extends Control
class_name CreditsScreen


signal back_pressed

@export
var FocusButton: Button


func focus():
	FocusButton.grab_focus()
