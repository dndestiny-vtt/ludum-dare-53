extends Control
class_name PauseScreen


signal resume_pressed
signal title_pressed

@export
var FocusButton: Button


func focus():
	FocusButton.grab_focus()
