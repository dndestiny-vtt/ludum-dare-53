extends Control
class_name FlashMessage


@onready
var _animation_player := $AnimationPlayer as AnimationPlayer

@onready
var _message := $Message as Label


func flash(message: String):
	self._message.text = message
	self._animation_player.play("ui/flash_message")
