extends Control
class_name GameOverScreen


const CONTENT_TEXT := "You got %d points!"

signal restart_pressed
signal title_pressed

@export
var FocusButton: Button

@export
var content_text: Label


func focus():
	FocusButton.grab_focus()


var points := 0:
	set(value):
		points = value
		self.content_text.text = CONTENT_TEXT % points
